<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//2
        DB::table('categories')->insert([
            'name' => 'Analgesik Narkotik',
            'description(additional)' => 'Golongan obat ini umumnya digunakan untuk meredakan atau menghilangkan rasa nyeri yang sifatnya sedang hingga berat seperti pada kedaan fractur atau patah tulang dan kanker.',
        ]);
//3
        DB::table('categories')->insert([
            'name' => 'Analgesik Non Narkotik',
            'description(additional)' => 'Obat Analgetik Non-Narkotik ini mampu menghilangkan atau meringankan rasa sakit tanpa berpengaruh pada sistem susunan saraf pusat atau bahkan hingga efek menurunkan tingkat kesadaran.',
        ]);
//4
        DB::table('categories')->insert([
            'name' => 'Nyeri Neuropatik',
            'description(additional)' => '',
        ]);
//5
        DB::table('categories')->insert([
            'name' => 'Anestetik Lokal',
            'description(additional)' => '',
        ]);
//6
        DB::table('categories')->insert([
            'name' => 'Anestetik Umum dan Oksigen',
            'description(additional)' => '',
        ]);
//7
        DB::table('categories')->insert([
            'name' => 'Obat untuk Prosedur Pre Operatif',
            'description(additional)' => '',
        ]);
//8
        DB::table('categories')->insert([
            'name' => 'Antialergi dan Obat untuk Anafilaksis',
            'description(additional)' => '',
        ]);
//9
        DB::table('categories')->insert([
            'name' => 'Antelmintik',
            'description(additional)' => '',
        ]);
//10
        DB::table('categories')->insert([
            'name' => 'Antibakteri',
            'description(additional)' => '',
        ]);
//11
        DB::table('categories')->insert([
            'name' => 'Antiinfeksi Khusus',
            'description(additional)' => '',
        ]);
    }
}
